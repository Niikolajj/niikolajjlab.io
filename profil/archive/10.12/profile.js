var jsonApp;
function loadFile()
{
    var input, file, fr;
    if (typeof window.FileReader !== 'function') 
    {
        alert("The file API isn't supported on this browser yet.");
        return;
    }
    input = document.getElementById('fileInput');
    if (!input) {
        alert("Couldn't find the fileInput element.");
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
    }
    else 
    {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
    }
    function receivedText(e) 
    {
        let lines = e.target.result;
        jsonApp = JSON.parse(lines); 

        createLetter();
        createProf();
    }
}
function createLetter()
{
    var page = document.createElement("page");
    document.body.appendChild(page);
    page.setAttribute("size", "A4");
    page.setAttribute("id", "pageLetter")
    var div = addElement("div", page, "full", "full");
    createSender(div);
    createReceiver(div);
    createDate(div);
    createSubject(div);
    createText(div);
    createGreetings(div);
}
function createProf()
{
    var page = document.createElement("page");
    document.body.appendChild(page);
    page.setAttribute("size", "A4");
    page.setAttribute("id", "pageProfile");
    var div = addElement("div", page, "full", "full");

    createProfileHeader(div);
    var profileF = addElement("div", div, "profile-f");
    var field = addElement("div", profileF,"profile-c", "profileContent");

    createDataSection(field);
    createStrength(field);
    createExperience(field);
    createInterest(field);
}
function createSender(target)
{
    var sender = addElement("div",target,"sender");
    target.appendChild(sender);
    var per = jsonApp.persons[0];
    var left= document.createElement("div");
    left.setAttribute("id", "send-left");
    addTextElement(per.firstName + " " + per.lastName, "h3", left);
    addTextElement(per.adress.street + " " + per.adress.houseNr, "h3", left);
    addTextElement(per.contact.phone, "h3", left);
    sender.appendChild(left);
    var right= document.createElement("div");
    right.setAttribute("id", "send-right");
    //var field = document.getElementById('send-right');
    addTextElement("| " + per.adress.postcode + " " + per.adress.city, "h3", right);
    addTextElement("| " + per.contact.email, "h3", right);
    sender.appendChild(right);
}
function createReceiver(target)
{
    var per = jsonApp.persons[1];
    var receiver = addElement("div", target, "receiver");
    addTextElement(per.company, "h3", receiver);
    addTextElement(per.firstName + " " + per.lastName,"h3", receiver);
    addTextElement(per.adress.street + " " + per.adress.houseNr,"h3", receiver);
    addTextElement(per.adress.postcode + " " + per.adress.city, "h3", receiver);
}
function createDate(target)
{
    var field = addElement("div",target,"date");
    addTextElement(jsonApp.persons[0].adress.city + ", " + jsonApp.application.date, "h3", field);
}
function createSubject(target)
{
    var field = addElement("div", target, "subject");
    addTextElement("Bewerbung als " +jsonApp.application.position, "span", field);
}
function createText(target)
{
    var field = addElement("div", target, "text");
    var gender = (jsonApp.persons[1].sex=="female")? " Frau " : "r Herr ";
    addTextElement("Sehr geehrte" + gender + jsonApp.persons[1].lastName +",","span", field);
    var jA = jsonApp.application;
    addTextElement(jA.introduction, "p", field);
    addTextElement(jA.interest, "p", field);
    addTextElement(jA.qualifications, "p", field);
    addTextElement(jA.invitability, "p", field);
}
function createGreetings(target)
{
    var field = addElement("div", target, "greetings");
    addTextElement(jsonApp.application.greetings, "p", field);
    addTextElement(jsonApp.persons[0].firstName + " " + jsonApp.persons[0].lastName, "span", field);
}
function createProfileHeader(target)
{
    var field = addElement("div", target, "header-f", "header");
    addTextElement("Kurzprofil", "h3", field, "profileHeader-l");
    addTextElement(jsonApp.persons[0].firstName + "." +jsonApp.persons[0].lastName, "h3", field, "profileHeader-r");
}
function createProfile()
{
    var field = document.getElementById("profile-f");
    var div = document.createElement("div");
    div.setAttribute("id", "profile-c");
    div.setAttribute("class", "profileContent");
    field.appendChild(div);

}
function createDataSection(target)
{
    addSplitSection("personal",target);
    var interestR = document.getElementById("personalR");
    
    var table = document.createElement("table");
    interestR.appendChild(table);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i",td,"", "far fa-calendar");
    addTextElement("Geb. " + jsonApp.persons[0].birth.date + " in " + jsonApp.persons[0].birth.place, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i", td, "" ,"fas fa-map-marker-alt");
    addTextElement(jsonApp.persons[0].adress.street + " " + jsonApp.persons[0].adress.houseNr + " " + jsonApp.persons[0].adress.postcode + " " +jsonApp.persons[0].adress.city, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i", td, "","fas fa-phone");
    addTextElement(jsonApp.persons[0].contact.phone, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i" ,td, "","fas fa-envelope");
    addTextElement(jsonApp.persons[0].contact.email, "td", tableRow);
}
function createStrength()
{
    var field = document.getElementById("profile-c");
    addSplitSection("strength",field);
    var strengthR = document.getElementById("strengthR");

    var list = document.createElement("ul");
    strengthR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.strength[i], "li", list);
    }
}
function createExperience()
{
    var field = document.getElementById("profile-c");
    addSplitSection("experience",field);
    var experienceR = document.getElementById("experienceR");

    var list = document.createElement("ul");
    experienceR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.experience[i], "li", list);
    }
}
function createInterest()
{
    var field = document.getElementById("profile-c");
    addSplitSection("interest",field);
    var interestR = document.getElementById("interestR");
    var list = document.createElement("ul");
    interestR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.interest[i], "li", list);
    }
}

function addTextElement(inText, elementType, target, idTag, className)
{
    var text = document.createElement(elementType);
    if(inText)text.textContent = inText;
    if(idTag)text.setAttribute("id", idTag);
    if(className)text.setAttribute("class", className);
    target.appendChild(text);
    return text;
}
function addElement(elementType, target, idTag,className)
{
    return addTextElement("",elementType,target,idTag,className);
}
function addSplitSection(name, target)
{
    var section = document.createElement("div");
    section.setAttribute("id", name);
    section.setAttribute("class", "section");
    target.appendChild(section);
    var sectionL = document.createElement("div");
    sectionL.setAttribute("id", name+"L");
    sectionL.setAttribute("class", "section-L");
    section.appendChild(sectionL);
    var sectionR = document.createElement("div");
    sectionR.setAttribute("id", name+"R");
    sectionR.setAttribute("class", "section-R");
    section.appendChild(sectionR);
}