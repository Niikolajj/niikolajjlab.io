var jsonApp;
var color;
function loadFile()
{
    var input, file, fr;
    if (typeof window.FileReader !== 'function') 
    {
        alert("The file API isn't supported on this browser yet.");
        return;
    }
    input = document.getElementById('fileInput');
    if (!input) {
        alert("Couldn't find the fileInput element.");
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
    }
    else 
    {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
        color = document.getElementById("colorBox").checked;
    }
    function receivedText(e) 
    {
        let lines = e.target.result;
        jsonApp = JSON.parse(lines); 
        hideInputs();
        createLetter(document.body);
        createProf(document.body);
        createCV(document.body);
        var cColor = jsonApp.persons[1].companycolor;
        if(!color && cColor!="")
        {
            elements = document.getElementsByClassName("accent");
            for (var i = 0; i < elements.length; i++) {
                elements[i].style.backgroundColor=cColor;
            }
        }
        else
        {
            elements = document.getElementsByClassName("accent");
            for (var i = 0; i < elements.length; i++) {
                elements[i].style.webkitAnimationName= "color-change";
                elements[i].style.webkitAnimationDuration= "360s";
            }
        }
    }
    function receivedPicture(e)
    {
        let picture = e.target.result;
        var img = addElement("img", document.getElementById("profilePic"));
        img.setAttribute("src", picture);
    }

}
function hideInputs()
{
    document.body.innerHTML = '';
    //document.getElementById("theFileScreen01").input.style.display = none;
}
function createLetter(target)
{
    var page = document.createElement("page");
    target.appendChild(page);
    page.setAttribute("size", "A4");
    page.setAttribute("id", "pageLetter")
    var div = addElement("div", page, "full", "full");
    var header = addElement("div", div, "header");
    var text= addElement("div", div, "textArea", "padded");
    createSender(header);
    createReceiver(header);
    createDate(header);
    createSubject(text);
    createText(text);
    createGreetings(text);
}
function createProf(target)
{
    var page = document.createElement("page");
    target.appendChild(page);
    page.setAttribute("size", "A4");
    page.setAttribute("id", "pageProfile");
    var div = addElement("div", page, "full", "full");

    createProfileHeader(div);
    var profileF = addElement("div", div, "profile-f");
    var field = addElement("div", profileF,"profile-c", "profileContent");

    createDataSection(field);
    createStrength(field);
    createExperience(field);
    createInterest(field);
}
function createCV(target)
{
    var page = document.createElement("page");
    target.appendChild(page);
    page.setAttribute("size", "A4");
    page.setAttribute("id", "pageCV");

    var div = addElement("div", page, "full", "full");
    var cv = addElement("div",div,"cv", "profileContent")
    createCVHeader(cv);
    createAcademia(cv);
    createEducation(cv);
    createInternship(cv);
    createLanguages(cv);
}
function createSender(target)
{
    var sender = addElement("div",target,"sender", "inverted padded");
    target.appendChild(sender);
    var per = jsonApp.persons[0];
    var left= document.createElement("div");
    left.setAttribute("id", "send-left");
    addTextElement(per.firstName + " " + per.lastName, "h3", left);
    addTextElement(per.adress.street + " " + per.adress.houseNr, "h3", left);
    addTextElement(per.contact.phone, "h3", left);
    sender.appendChild(left);
    var right= document.createElement("div");
    right.setAttribute("id", "send-right");
    //var field = document.getElementById('send-right');
    addTextElement("| " + per.adress.postcode + " " + per.adress.city, "h3", right);
    addTextElement("| " + per.contact.email, "h3", right);
    sender.appendChild(right);
}
function createReceiver(target)
{
    var per = jsonApp.persons[1];
    var receiver = addElement("div", target, "receiver", "padded");
    addTextElement(per.company, "h3", receiver);
    if(per.gender != "unknown")addTextElement(per.firstName + " " + per.lastName,"h3", receiver);
    addTextElement(per.adress.street + " " + per.adress.houseNr,"h3", receiver);
    addTextElement(per.adress.postcode + " " + per.adress.city, "h3", receiver);
}
function createDate(target)
{
    var field = addElement("div",target,"date","padded");
    addTextElement(jsonApp.persons[0].adress.city + ", " + jsonApp.application.date, "h3", field);
}
function createSubject(target)
{
    var field = addElement("div", target, "subject");
    addTextElement("Bewerbung als " +jsonApp.application.position, "span", field);
}
function createText(target)
{
    var field = addElement("div", target, "text");
    var gender = (jsonApp.persons[1].sex=="female")? " Frau " : "r Herr ";
    if(jsonApp.persons[1].sex=="unknown")
    {
        addTextElement("Sehr geehrte Damen und Herren" +",","span", field);
    }
    else
    {
        addTextElement("Sehr geehrte" + gender + jsonApp.persons[1].lastName +",","span", field);
    }
    var jA = jsonApp.application;
    addTextElement(jA.introduction, "p", field);
    addTextElement(jA.interest, "p", field);
    addTextElement(jA.qualifications, "p", field);
    addTextElement(jA.invitability, "p", field);
}
function createGreetings(target)
{
    var field = addElement("div", target, "greetings");
    addTextElement(jsonApp.application.greetings, "p", field);
    addTextElement(jsonApp.persons[0].firstName + " " + jsonApp.persons[0].lastName, "span", field);
}
function createProfileHeader(target)
{
    var field = addElement("div", target, "header-f", "header inverted");
    addTextElement("Kurzprofil", "h2", field, "profileHeader-l");
    addTextElement(jsonApp.persons[0].firstName + "." +jsonApp.persons[0].lastName, "h2", field, "profileHeader-r");
}
function createProfile()
{
    var field = document.getElementById("profile-f");
    var div = document.createElement("div");
    div.setAttribute("id", "profile-c");
    div.setAttribute("class", "profileContent");
    field.appendChild(div);

}
function createDataSection(target)
{
    addSplitSection("personal",target);

    var interestL = document.getElementById("personalL");
    var picArea = addElement("div", interestL, "profilePic", "round accent");
    var picture = addElement("img", picArea, "profilePicture");
    picture.setAttribute("src", "disguiseddata")
    var interestR = document.getElementById("personalR");
    
    var table = document.createElement("table");
    interestR.appendChild(table);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i",td,"", "far fa-calendar");
    addTextElement("Geb. " + jsonApp.persons[0].birth.date + " in " + jsonApp.persons[0].birth.place, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i", td, "" ,"fas fa-map-marker-alt");
    addTextElement(jsonApp.persons[0].adress.street + " " + jsonApp.persons[0].adress.houseNr + " " + jsonApp.persons[0].adress.postcode + " " +jsonApp.persons[0].adress.city, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i", td, "","fas fa-phone");
    addTextElement(jsonApp.persons[0].contact.phone, "td", tableRow);

    var tableRow = document.createElement("tr");
    table.appendChild(tableRow);
    var td = addElement("td",tableRow);
    addElement("i" ,td, "","fas fa-envelope");
    addTextElement(jsonApp.persons[0].contact.email, "td", tableRow);
}
function createStrength()
{
    var field = document.getElementById("profile-c");

    addSplitSection("strengthTitle", field);
    var strengthL = document.getElementById("strengthTitleL");
    addTextElement("Stärken", "h1", strengthL, "strengthL", "title accent");

    addSplitSection("strength",field);
    var strengthR = document.getElementById("strengthR");

    var list = document.createElement("ul");
    strengthR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.strength[i], "li", list);
    }
}
function createExperience()
{
    var field = document.getElementById("profile-c");

    addSplitSection("experienceTitle", field);
    var experienceL = document.getElementById("experienceTitleL");
    addTextElement("Erfahrungen", "h1", experienceL, "experienceL", "title accent");

    addSplitSection("experience",field);
    var experienceR = document.getElementById("experienceR");

    var list = document.createElement("ul");
    experienceR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.experience[i], "li", list);
    }
}
function createInterest()
{
    var field = document.getElementById("profile-c");

    addSplitSection("interestTitle", field);
    var interestL = document.getElementById("interestTitleL");
    addTextElement("Interessen", "h1", interestL, "interestL", "title accent");

    addSplitSection("interest",field);
    var interestR = document.getElementById("interestR");
    var list = document.createElement("ul");
    interestR.appendChild(list);
    for(var i=0;i<4;i++)
    {
        addTextElement(jsonApp.persons[0].profile.interest[i], "li", list);
    }
}
function createCVHeader(target)
{
    var field = addElement("div", target, "CVheader-f", "header inverted");
    addTextElement("Lebenslauf", "h2", field, "CVHeader-l", "header-l");
    addTextElement(jsonApp.persons[0].firstName + "." +jsonApp.persons[0].lastName, "h2", field, "CVHeader-r", "header-r");
}
function createAcademia(target)
{
    addSplitSection("academiaTitle", target);
    academiaTitleL = document.getElementById("academiaTitleL");
    addTextElement("Studium", "h1", academiaTitleL, "academiaTitleL", "title accent");

    addSplitSection("academia", target);
    academiaL = document.getElementById("academiaL");
    addTimeline(jsonApp.persons[0].cv.academia, academiaR);
}
function createEducation(target)
{
    addSplitSection("educationTitle", target);
    educationTitleL = document.getElementById("educationTitleL");
    addTextElement("Schulbildung", "h1", educationTitleL, "educationTitleL", "title accent");

    addSplitSection("education", target);
    educationL = document.getElementById("educationL");
    addTimeline(jsonApp.persons[0].cv.education, educationR);
}
function createInternship(target)
{
    addSplitSection("internshipTitle", target);
    internshipTitleL = document.getElementById("internshipTitleL");
    addTextElement("Praktika", "h1", internshipTitleL, "internshipTitleL", "title accent");

    addSplitSection("internship", target);
    internshipL = document.getElementById("internshipL");
    addTimes(jsonApp.persons[0].cv.internship, internshipR);
}
function createLanguages(target)
{
    addSplitSection("languageTitle", target);
    languageTitleL = document.getElementById("languageTitleL");
    addTextElement("Sprachen", "h1", languageTitleL, "languageTitleL", "title accent");

    addSplitSection("language", target);
    languageL = document.getElementById("languageL");
    addLanguages(jsonApp.persons[0].cv.languages, languageR);
}
function addTextElement(inText, elementType, target, idTag, className)
{
    var text = document.createElement(elementType);
    if(inText)text.textContent = inText;
    if(idTag)text.setAttribute("id", idTag);
    if(className)text.setAttribute("class", className);
    target.appendChild(text);
    return text;
}
function addElement(elementType, target, idTag,className)
{
    return addTextElement("",elementType,target,idTag,className);
}
function addSplitSection(name, target)
{
    var section = document.createElement("div");
    section.setAttribute("id", name);
    section.setAttribute("class", "section");
    target.appendChild(section);
    var sectionL = document.createElement("div");
    sectionL.setAttribute("id", name+"L");
    sectionL.setAttribute("class", "section-L");
    section.appendChild(sectionL);
    var sectionR = document.createElement("div");
    sectionR.setAttribute("id", name+"R");
    sectionR.setAttribute("class", "section-R");
    section.appendChild(sectionR);
}
function addTimeline(content, target)
{
    var timeline = addElement("table",target,"", "timeline");
    for(var i=0; i < content.length; i++)
    {
        if(content[i].type=="data")
        {
            addTlData(content[i], timeline);
        }
        else if(content[i].type = "date")
        {
            addTlIndex(content[i], timeline);
        }
    }
}
function addTimes(content, target)
{
    var times = addElement("table",target,"", "times");
    for(var i=0; i < content.length; i++)
    {
        var tr = addElement("tr",times);
        addTextElement(content[i].date,"td",tr, "", "tlDate");
        var data = addElement("td",tr);
        addTextElement(content[i].place + " - " + content[i].location,"div",data);
        addTextElement(content[i].occupation, "div", data);
        if(content[i].content)addTextElement(content[i].content, "div", data);
    }
}
function addTlIndex(content, target)
{
    var tr = addElement("tr",target,"", "tlIndexRow");
    var td = addElement("td",tr,"","tlIndex")
    addTextElement(content.content,"div",td,"","tlDate");
    
}
function addTlData(content, target)
{
    var tr = addElement("tr",target,"", "tlDataRow");
    addElement("td",tr,"","");
    var td = addElement("td",tr,"","tlData");
    addTextElement(content.place + " - " + content.location,"div",td);
    addTextElement(content.degree,"div",td);
    if(content.area)addTextElement(content.area,"div",td);
    if(content.content)addTextElement(content.content,"div",td);
}
function addLanguages(content, target)
{
    var languages = addElement("ul",target,"", "languages");
    var languageScores = addElement("div", target, "","languageScores");
    for(var i=0; i < content.length; i++)
    {
        addTextElement(content[i].type,"li",languages,"","languageEntries");
        var div = addElement("div", languageScores,"","languageScore");
        for(var j=0; j < 6; j++)
        {
            if(j < content[i].score)addElement("i",div,"","fas fa-star");
            else addElement("i",div,"","far fa-star");
        }
    }
}