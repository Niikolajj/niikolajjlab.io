const cmds = {
    twitter:'https://twitter.com/niikolajj',
    spotify:'https://open.spotify.com/user/niikolajj'
};

const cli = document.getElementById('cli');

cli.addEventListener('keypress', function (e) {
    if (e.key === "Enter") {
        let cmd = cmds[cli.value];
        if(cmd)
        {
            window.location.href = cmd;
        }
        else {
            cli.value = "";
            const commandline = document.getElementById('commandline');
            //commandline.classList.add('shake');
            commandline.animate([{ transform: 'translate(0px, 0px)' },
            { transform: 'translate(4px, 0px)' },
            { transform: 'translate(-8px, 0px)' },
            { transform: 'translate(0px, 0px)' }], 250);
        }
    }
});
